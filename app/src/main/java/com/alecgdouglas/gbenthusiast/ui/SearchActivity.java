package com.alecgdouglas.gbenthusiast.ui;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v17.leanback.app.RowsFragment;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.CursorObjectAdapter;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ImageCardView;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.OnItemViewClickedListener;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.Row;
import android.support.v17.leanback.widget.RowPresenter;
import android.support.v17.leanback.widget.SearchOrbView;
import android.support.v17.leanback.widget.VerticalGridView;
import android.support.v4.app.ActivityOptionsCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.alecgdouglas.gbenthusiast.ApiKeyManager;
import com.alecgdouglas.gbenthusiast.R;
import com.alecgdouglas.gbenthusiast.Utils;
import com.alecgdouglas.gbenthusiast.data.VideoContract;
import com.alecgdouglas.gbenthusiast.model.Video;
import com.alecgdouglas.gbenthusiast.model.VideoCursorMapper;
import com.alecgdouglas.gbenthusiast.presenter.VideoCardPresenter;

public class SearchActivity extends CommonActivity
        implements LoaderManager.LoaderCallbacks<Cursor> {
    /* Transitions are a bit janky due to how the SearchOrb animates.
    public static final String SHARED_SEARCH_ORB_CIRCLE_NAME = "search_orb_view";
    public static final String SHARED_SEARCH_ORB_ICON_NAME = "search_icon_view";
    */
    private static final int QUERY_LOADER = 1;
    private static final String QUERY_KEY = "query";
    private final CursorObjectAdapter mVideoCursorAdapter =
            new CursorObjectAdapter(new VideoCardPresenter());
    private EditText mSearchText;
    private SearchOrbView mSearchOrb;
    private RowsFragment mRowsFragment;
    private ArrayObjectAdapter mRowsAdapter;
    private String mQuery;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        final VerticalGridView vgv = (VerticalGridView) findViewById(R.id.result_rows);
        vgv.setWindowAlignmentOffsetPercent(10f);

        mSearchText = (EditText) findViewById(R.id.search_text);
        mSearchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                startQuery(s.toString());
            }
        });

        final View searchOrbCircle = findViewById(android.support.v17.leanback.R.id.search_orb);
        ((ViewGroup.MarginLayoutParams) searchOrbCircle.getLayoutParams())
                .setMargins(15, 15, 15, 15);
        /* This transition is a bit janky due to how the SearchOrb animates.
        if (!Utils.isLowEndDevice()) {
            searchOrbCircle.setTransitionName(SHARED_SEARCH_ORB_CIRCLE_NAME);
            findViewById(android.support.v17.leanback.R.id.icon)
                    .setTransitionName(SHARED_SEARCH_ORB_ICON_NAME);
        }*/

        mSearchOrb = (SearchOrbView) findViewById(R.id.search_orb_view);
        mSearchOrb.setOrbIcon(getResources().getDrawable(R.drawable.ic_search, null));
        mSearchOrb.setOrbColors(
                new SearchOrbView.Colors(getResources().getColor(R.color.brand_color_highlight),
                        getResources().getColor(R.color.brand_color_highlight)));
        mSearchOrb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchText.requestFocus();
                final InputMethodManager imm =
                        (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.showSoftInput(mSearchText, 0);
            }
        });
        mRowsFragment = (RowsFragment) getFragmentManager().findFragmentById(R.id.result_rows);

        mRowsAdapter = new ArrayObjectAdapter(new ListRowPresenter());
        mVideoCursorAdapter.setMapper(new VideoCursorMapper(ApiKeyManager.getApiKey(this)));

        mRowsFragment.setAdapter(mRowsAdapter);
        mRowsFragment.setOnItemViewClickedListener(new ItemViewClickedListener());
    }

    private void startQuery(String query) {
        mQuery = query;
        getLoaderManager().destroyLoader(QUERY_LOADER);
        if (!TextUtils.isEmpty(query)) {
            final Bundle args = new Bundle();
            args.putString(QUERY_KEY, query);
            getLoaderManager().initLoader(QUERY_LOADER, args, this);
        } else {
            mRowsAdapter.clear();
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        final int titleRes = (cursor != null && cursor.moveToFirst()) ? R.string.search_results :
                R.string.no_search_results;
        mVideoCursorAdapter.changeCursor(cursor);
        final HeaderItem header = new HeaderItem(getString(titleRes, mQuery));
        mRowsAdapter.clear();
        final ListRow row = new ListRow(header, mVideoCursorAdapter);
        mRowsAdapter.add(row);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mVideoCursorAdapter.changeCursor(null);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        final String query = args.getString(QUERY_KEY);
        return new CursorLoader(this, VideoContract.VideoEntry.CONTENT_URI, null, // Return all
                // fields.
                VideoContract.VideoEntry.COLUMN_NAME + " LIKE ? OR " +
                        VideoContract.VideoEntry.COLUMN_DECK + " LIKE ?",
                new String[] {"%" + query + "%", "%" + query + "%"},
                "datetime(" + VideoContract.VideoEntry.COLUMN_PUBLISH_DATE + ") DESC");
    }

    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item,
                                  RowPresenter.ViewHolder rowViewHolder, Row row) {
            if (item instanceof Video) {
                final Video video = (Video) item;
                final Intent intent = new Intent(SearchActivity.this, VideoDetailsActivity.class);
                intent.putExtra(VideoDetailsActivity.VIDEO, video);

                if (Utils.isLowEndDevice()) {
                    SearchActivity.this.startActivity(intent);
                } else {
                    final Bundle bundle = ActivityOptionsCompat
                            .makeSceneTransitionAnimation(SearchActivity.this,
                                    ((ImageCardView) itemViewHolder.view).getMainImageView(),
                                    VideoDetailsActivity.SHARED_ELEMENT_NAME).toBundle();
                    SearchActivity.this.startActivity(intent, bundle);
                }
            }
        }
    }
}
