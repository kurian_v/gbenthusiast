package com.alecgdouglas.gbenthusiast.ui;

import android.app.Fragment;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.alecgdouglas.gbenthusiast.R;

public class SpinnerFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final FrameLayout layout = new FrameLayout(container.getContext());
        if (container instanceof FrameLayout) {
            View dimView = new View(container.getContext());
            ProgressBar progressBar = new ProgressBar(container.getContext());
            Resources res = getResources();
            int width = res.getDimensionPixelSize(R.dimen.spinner_width);
            int height = res.getDimensionPixelSize(R.dimen.spinner_width);
            progressBar
                    .setLayoutParams(new FrameLayout.LayoutParams(width, height, Gravity.CENTER));

            dimView.setLayoutParams(
                    new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                            FrameLayout.LayoutParams.MATCH_PARENT));
            dimView.setBackgroundColor(Color.BLACK);
            dimView.setAlpha(0.5f);

            layout.addView(dimView);
            layout.addView(progressBar);
        }
        return layout;
    }
}