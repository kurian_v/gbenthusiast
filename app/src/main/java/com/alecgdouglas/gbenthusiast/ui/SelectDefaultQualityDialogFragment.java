package com.alecgdouglas.gbenthusiast.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.alecgdouglas.gbenthusiast.DefaultQualityPrefUtils;
import com.alecgdouglas.gbenthusiast.R;
import com.alecgdouglas.gbenthusiast.model.VideoQuality;

public class SelectDefaultQualityDialogFragment extends DialogFragment {
    private static final String TAG = "SelectDefQualDialog";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.select_default_quality_dialog_title)
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        final ArrayAdapter<String> qualities =
                new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_singlechoice);
        for (VideoQuality quality : VideoQuality.values()) {
            qualities.add(quality.toString());
        }

        final VideoQuality currentQuality =
                DefaultQualityPrefUtils.getDefaultQuality(getActivity());
        int currentQualityPosition = qualities.getPosition(currentQuality.toString());
        currentQualityPosition = Math.max(currentQualityPosition, 0);

        builder.setSingleChoiceItems(qualities, currentQualityPosition,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final String selectedQualityString = qualities.getItem(which);
                        try {
                            final VideoQuality selectedQuality =
                                    VideoQuality.valueOf(selectedQualityString);
                            DefaultQualityPrefUtils
                                    .setDefaultQuality(getActivity(), selectedQuality);
                        } catch (IllegalArgumentException | NullPointerException e) {
                            Log.w(TAG, "Selected quality is not valid: " + selectedQualityString,
                                    e);
                        } finally {
                            dialog.dismiss();
                        }
                    }
                });
        final AlertDialog alertDialog = builder.create();
        return alertDialog;
    }
}
