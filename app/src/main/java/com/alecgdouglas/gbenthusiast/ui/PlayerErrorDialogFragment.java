package com.alecgdouglas.gbenthusiast.ui;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.alecgdouglas.gbenthusiast.R;
import com.google.android.exoplayer2.ExoPlaybackException;

public class PlayerErrorDialogFragment extends DialogFragment {
    private ExoPlaybackException mError;

    public PlayerErrorDialogFragment() {
    }

    public void setError(ExoPlaybackException error) {
        mError = error;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AppInfoDialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.app_info_dialog_fragment, container, false);

        Exception exception;
        switch (mError.type) {
            case ExoPlaybackException.TYPE_SOURCE:
                exception = mError.getSourceException();
                break;
            case ExoPlaybackException.TYPE_RENDERER:
                exception = mError.getRendererException();
                break;
            case ExoPlaybackException.TYPE_UNEXPECTED:
                exception = mError.getUnexpectedException();
                break;
            default:
                exception = new Exception(
                        getActivity().getResources().getString(R.string.player_error_unknown));
                break;
        }

        final String message = getActivity().getResources()
                .getString(R.string.player_error_dialog_message, exception.getMessage());

        final TextView messageView = (TextView) rootView.findViewById(R.id.message);
        messageView.setText(message);

        final Button dismissButton = (Button) rootView.findViewById(R.id.dismiss_button);
        dismissButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                dismiss();
                getActivity().finish();
            }
        });

        return rootView;
    }
}
