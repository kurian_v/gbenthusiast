package com.alecgdouglas.gbenthusiast.ui;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v17.leanback.widget.ImageCardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alecgdouglas.gbenthusiast.R;
import com.alecgdouglas.gbenthusiast.presenter.DrawableItem;

public class DrawableGridItem implements DrawableItem{
    // Unique ID for distinguishing items for executing actions when they are clicked on.
    private final int mId;
    private final String mText;
    private final int mDrawableResourceId;

    public DrawableGridItem(int id, String text, int drawableResourceId) {
        mId = id;
        mText = text;
        mDrawableResourceId = drawableResourceId;
    }

    public int getId() {
        return mId;
    }

    public String getText() {
        return mText;
    }

    public int getDrawableResourceId() {
        return mDrawableResourceId;
    }

    @Override
    public void drawItem(ImageCardView cardView, Object item) {
        cardView.setTitleText(getText());
        final ImageView mainImageView = cardView.getMainImageView();
        mainImageView.setImageResource(getDrawableResourceId());
        mainImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        mainImageView.setImageAlpha(160);
        mainImageView.getDrawable().setTint(Color.WHITE);
        final int padding =
                cardView.getResources().getDimensionPixelSize(R.dimen.drawable_image_card_padding);
        mainImageView.setPadding(padding, padding, padding, padding);

        // Center the title and hide the content text if there is no content text.
        if (cardView.getContentText() == null || cardView.getContentText().toString().isEmpty()) {
            final TextView titleView = (TextView) cardView.getRootView()
                    .findViewById(android.support.v17.leanback.R.id.title_text);
            titleView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            final View contentView = cardView.getRootView()
                    .findViewById(android.support.v17.leanback.R.id.content_text);
            if (contentView != null) {
                contentView.setVisibility(View.GONE);
            }
        }
    }
}

