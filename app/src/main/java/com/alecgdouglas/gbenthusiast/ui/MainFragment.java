/*
 * This file was copied from the Android TV Leanback sample project and modified by Alec Douglas.
 * https://github.com/googlesamples/androidtv-Leanback
 * The original file included the copyright notice below:
 *
 * Copyright (c) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.alecgdouglas.gbenthusiast.ui;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.support.v17.leanback.app.BackgroundManager;
import android.support.v17.leanback.app.BrowseFragment;
import android.support.v17.leanback.app.ErrorFragment;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.CursorObjectAdapter;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ImageCardView;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.OnItemViewClickedListener;
import android.support.v17.leanback.widget.OnItemViewSelectedListener;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.PresenterSelector;
import android.support.v17.leanback.widget.Row;
import android.support.v17.leanback.widget.RowPresenter;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;

import com.alecgdouglas.gbenthusiast.ApiGetRequestParams;
import com.alecgdouglas.gbenthusiast.ApiGetRequestTask;
import com.alecgdouglas.gbenthusiast.ApiKeyManager;
import com.alecgdouglas.gbenthusiast.DefaultQualityPrefUtils;
import com.alecgdouglas.gbenthusiast.LastRefreshTimePrefUtils;
import com.alecgdouglas.gbenthusiast.LiveCheckTask;
import com.alecgdouglas.gbenthusiast.R;
import com.alecgdouglas.gbenthusiast.Utils;
import com.alecgdouglas.gbenthusiast.VideoPositionRowUpdateUtils;
import com.alecgdouglas.gbenthusiast.data.VideoContract;
import com.alecgdouglas.gbenthusiast.data.VideoDbHelper;
import com.alecgdouglas.gbenthusiast.data.VideoFetchResultReceiver;
import com.alecgdouglas.gbenthusiast.data.VideoFetchService;
import com.alecgdouglas.gbenthusiast.data.VideoPositionDbUtils;
import com.alecgdouglas.gbenthusiast.model.Video;
import com.alecgdouglas.gbenthusiast.model.VideoCursorMapper;
import com.alecgdouglas.gbenthusiast.presenter.CardPresenter;
import com.alecgdouglas.gbenthusiast.presenter.DrawableImageCardPresenter;
import com.alecgdouglas.gbenthusiast.presenter.IconHeaderItemPresenter;
import com.alecgdouglas.gbenthusiast.presenter.VideoCardPresenter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Main class to show BrowseFragment with header and rows of videos
 */
public class MainFragment extends BrowseFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String TAG = "MainFragment";
    private static final long BACKGROUND_UPDATE_DELAY_MS = 300L;
    private static final long LIVE_CHECK_INTERVAL_MS = 2L * 60L * 1000L; // 2 minutes
    private static final int VIDEO_TYPE_LOADER = 1;
    private static final int MOST_RECENT_VIDEO_LOADER = 2;
    private static final int CONTINUE_WATCHING_VIDEO_LOADER = 3;
    private static final String REFRESH_SPINNER_TAG = "refresh_spinner";

    private static final int REFRESH_ITEM_ID = 1;
    private static final int CLEAR_AUTH_ITEM_ID = 2;
    private static final int SHOW_APP_INFO_ITEM_ID = 3;
    private static final int SELECT_DEFAULT_QUALITY_ITEM_ID = 4;
    private static final int INFINITE_ITEM_ID = 5;

    private final Handler mHandler = new Handler();
    private final ArrayObjectAdapter mVideoTypeRowAdapter =
            new ArrayObjectAdapter(new ListRowPresenter());
    // Maps a Loader Id to its CursorObjectAdapter.
    private final SparseArray<CursorObjectAdapter> mVideoCursorAdapters = new SparseArray<>();
    private Drawable mDefaultBackground;
    private DisplayMetrics mMetrics;
    private Timer mBackgroundTimer;
    private Timer mLiveCheckTimer;
    private Uri mBackgroundUri;
    private BackgroundManager mBackgroundManager;
    private boolean mAttached = false;
    private boolean mAdapterSet = false;
    private boolean mPendingFetch = false;
    // Keep track of whether a video is selected. This is used to ensure we're setting the
    // background appropriately since Glide can take a little while to update the background (see
    // BACKGROUND_UPDATE_DELAY_MS) and whether or not a video is selected can change in the
    // meantime.
    private boolean mIsVideoSelected = false;
    private int mLastSelectedPosition = -1;
    private final BroadcastReceiver mOnMenuKeyDownReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final int selectedPosition = getSelectedPosition();
            final int actionsPosition = getAdapter().size() - 1;

            // Attempting to smooth scroll is a bit janky even on high-end devices.
            final boolean smoothScroll = false; // !Utils.isLowEndDevice();
            if (selectedPosition == actionsPosition && mLastSelectedPosition >= 0) {
                setSelectedPosition(mLastSelectedPosition, smoothScroll);
            } else if (selectedPosition != actionsPosition) {
                mLastSelectedPosition = selectedPosition;
                setSelectedPosition(actionsPosition, smoothScroll);
            }
        }
    };
    private ListRow mContinueWatchingRow;
    private boolean mContinueWatchingNeedsUpdate = false;
    private final BroadcastReceiver mOnSavedVideoPositionUpdatedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final Video updatedVideo =
                    (Video) intent.getSerializableExtra(VideoPositionDbUtils.VIDEO_KEY);
            mContinueWatchingNeedsUpdate = VideoPositionRowUpdateUtils
                    .updateRows(updatedVideo, mVideoCursorAdapters, mVideoTypeRowAdapter,
                            Arrays.asList(MOST_RECENT_VIDEO_LOADER), Arrays.asList(
                                    getResources().getString(R.string.most_recent_videos_header)));
        }
    };
    private ListRow mActionsRow;
    private ListRow mLiveVideoRow;
    private Uri mInfiniteUrl;
    private boolean mInfiniteOnYoutube;
    private Video mSelectedVideo;
    private final BroadcastReceiver mOnPlayPauseKeyDownReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mSelectedVideo == null || isShowingHeaders()) {
                return;
            }

            startDetailsActivity(mSelectedVideo, null, true);
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onAttachInternal();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        onAttachInternal();
    }

    private void onAttachInternal() {
        if (mAttached) return;
        mAttached = true;

        if (LastRefreshTimePrefUtils.shouldRefresh(getActivity(), false)) {
            refreshVideos(false, false);
        } else {
            Log.d(TAG, "Not fetching videos from server, " + "loading videos from DB immediately " +
                    "" + "" + "" + "instead");
            getLoaderManager().initLoader(VIDEO_TYPE_LOADER, null, this);
        }
    }

    private void beginVideoFetch(final boolean manualRefresh) {
        if (mPendingFetch) {
            Log.w(TAG, "Pending video fetch, not running second fetch");
            return;
        }
        LastRefreshTimePrefUtils.update(getActivity());
        final ResultReceiver videoFetchResultReceiver = new VideoFetchResultReceiver(new Handler(),
                new VideoFetchResultReceiver.OnFetchCompleteHandler() {
                    @Override
                    public void onFetchComplete() {
                        Log.d(TAG, "Videos fetch is complete");
                        if (getActivity() == null) {
                            Log.d(TAG, "Main fragment has already been removed from the host, " +
                                    "not " + "restarting video loader");
                        } else {
                            Log.d(TAG, "Restarting video loader");
                            getLoaderManager()
                                    .restartLoader(VIDEO_TYPE_LOADER, null, MainFragment.this);
                        }

                        if (manualRefresh && getSelectedPosition() > 0) {
                            // Only set the selected position if the user manually refreshed videos
                            // (so they don't lose their position on a video category row) and it's
                            // already been set otherwise the UI won't load properly.
                            setSelectedPosition(0, false);
                        }
                        clearSpinners();
                        mPendingFetch = false;
                    }
                });
        final Intent fetchServiceIntent = new Intent(getActivity(), VideoFetchService.class);
        fetchServiceIntent
                .putExtra(VideoFetchService.EXTRA_KEY_RESULT_RECEIVER, videoFetchResultReceiver);
        mPendingFetch = true;
        getActivity().startService(fetchServiceIntent);
    }

    private boolean isSpinnerShowing() {
        return getFragmentManager().findFragmentByTag(REFRESH_SPINNER_TAG) != null;
    }

    private void addSpinner() {
        if (isSpinnerShowing()) return;
        final FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager == null) return;

        final SpinnerFragment spinnerFragment = new SpinnerFragment();
        fragmentManager.beginTransaction()
                .add(android.R.id.content, spinnerFragment, REFRESH_SPINNER_TAG).commit();
    }

    private void clearSpinners() {
        final FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager == null) return;
        Fragment spinnerFragment = fragmentManager.findFragmentByTag(REFRESH_SPINNER_TAG);
        while (spinnerFragment != null) {
            fragmentManager.beginTransaction().remove(spinnerFragment).commit();
            fragmentManager.executePendingTransactions();
            spinnerFragment = fragmentManager.findFragmentByTag(REFRESH_SPINNER_TAG);
        }
    }

    @Override
    public void onCreate(Bundle bundle) {
        registerReceivers();
        super.onCreate(bundle);
    }

    /**
     * Refresh the videos from the server and have the UI respond appropriately.
     *
     * @param manualRefresh Indicate whether or not this refresh is happening due to a direct action
     *                      by the user (i.e. a manual refresh) or automatically (e.g. triggered by
     *                      a lifecycle event). If true, focus will be brought back to the left
     *                      panel and the user's selection in a row will be lost.
     * @param background    Indicate whether or not this refresh is happening while this fragment
     *                      is in
     *                      the background, i.e. true if onPause/onStop has been called already.
     */
    private void refreshVideos(boolean manualRefresh, boolean background) {
        Log.i(TAG, "Refreshing videos from server");
        if (!background) {
            addSpinner();
        }
        beginVideoFetch(manualRefresh);
        // Force the live check to happen again.
        killLiveCheckTimer();
        startLiveCheckTimer();

        if (manualRefresh && !background) {
            // Bring focus back to the left panel instead of the rows if this is a manual refresh.
            startHeadersTransition(true);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // Final initialization, modifying UI elements.
        super.onActivityCreated(savedInstanceState);

        // Prepare the manager that maintains the same background image between activities.
        prepareBackgroundManager();

        setupUiElements();
        setupEventListeners();
        prepareEntranceTransition();
    }

    @Override
    public void onPause() {
        super.onPause();

        killLiveCheckTimer();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (LastRefreshTimePrefUtils.shouldRefresh(getActivity(), true)) {
            refreshVideos(false, false);
        } else if (mContinueWatchingNeedsUpdate) {
            Log.i(TAG, "Continue watching column needs an update, reloading row");
            mContinueWatchingNeedsUpdate = false;
            getLoaderManager().restartLoader(CONTINUE_WATCHING_VIDEO_LOADER, null, MainFragment
                    .this);
        }

        startLiveCheckTimer();
    }

    @Override
    public void onDestroy() {
        killBackgroundTimer();
        mBackgroundManager = null;
        unregisterReceivers();
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mBackgroundUri != null) {
            updateBackground(mBackgroundUri.toString());
        } else if (mBackgroundManager != null) {
            // Ensure we set the drawable to something to avoid a NPE in the BackgroundManager code
            // during the next animation update since we auto-release during onStop.
            mBackgroundManager.setDrawable(mDefaultBackground);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (LastRefreshTimePrefUtils.shouldRefresh(getActivity(), false)) {
            refreshVideos(false, true);
        }
    }

    private void registerReceivers() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mOnMenuKeyDownReceiver,
                new IntentFilter(MainActivity.ON_MENU_KEY_DOWN_BROADCAST_INTENT_KEY));
        LocalBroadcastManager.getInstance(getActivity())
                .registerReceiver(mOnPlayPauseKeyDownReceiver,
                        new IntentFilter(MainActivity.ON_PLAY_PAUSE_KEY_DOWN_BROADCAST_INTENT_KEY));
        LocalBroadcastManager.getInstance(getActivity())
                .registerReceiver(mOnSavedVideoPositionUpdatedReceiver, new IntentFilter(
                        VideoPositionDbUtils.ON_VIDEO_POSITION_UPDATED_BROADCAST_INTENT_KEY));
    }

    private void unregisterReceivers() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mOnMenuKeyDownReceiver);
        LocalBroadcastManager.getInstance(getActivity())
                .unregisterReceiver(mOnSavedVideoPositionUpdatedReceiver);
        LocalBroadcastManager.getInstance(getActivity())
                .unregisterReceiver(mOnPlayPauseKeyDownReceiver);
    }

    private void prepareBackgroundManager() {
        mBackgroundManager = BackgroundManager.getInstance(getActivity());
        mBackgroundManager.setAutoReleaseOnStop(true);
        mBackgroundManager.attach(getActivity().getWindow());
        mDefaultBackground = getResources().getDrawable(R.drawable.default_background, null);
        mMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(mMetrics);
    }

    private void setupUiElements() {
        setBadgeDrawable(getActivity().getResources().getDrawable(R.drawable.app_badge, null));
        setTitle(getString(R.string.browse_title));
        setHeadersState(HEADERS_ENABLED);
        setHeadersTransitionOnBackEnabled(true);

        // Set fastLane (or headers) background color
        setBrandColor(ContextCompat.getColor(getActivity(), R.color.fastlane_background));

        // Set search icon color.
        setSearchAffordanceColor(ContextCompat.getColor(getActivity(), R.color.search_opaque));

        setHeaderPresenterSelector(new PresenterSelector() {
            @Override
            public Presenter getPresenter(Object o) {
                return new IconHeaderItemPresenter();
            }
        });
    }

    private void setupEventListeners() {
        setOnSearchClickedListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getActivity(), SearchActivity.class);
                startActivity(intent);
                /* This transition is a bit janky due to how the SearchOrb animates.
                if (Utils.isLowEndDevice()) {
                    startActivity(intent);
                } else {
                    final View searchOrbCircle =
                            getActivity().findViewById(android.support.v17.leanback.R.id.title_orb);
                    final View searchOrbIcon =
                            getActivity().findViewById(android.support.v17.leanback.R.id.icon);
                    final Bundle bundle = ActivityOptions.makeSceneTransitionAnimation(
                            getActivity(),
                            Pair.create(searchOrbCircle,
                                    SearchActivity.SHARED_SEARCH_ORB_CIRCLE_NAME),
                            Pair.create(searchOrbIcon, SearchActivity.SHARED_SEARCH_ORB_ICON_NAME)
                    ).toBundle();
                    startActivity(intent, bundle);
                }*/
            }
        });

        setOnItemViewClickedListener(new ItemViewClickedListener());
        setOnItemViewSelectedListener(new ItemViewSelectedListener());
    }

    private void updateBackground(String uri) {
        if (mBackgroundManager == null) return;
        int width = mMetrics.widthPixels;
        int height = mMetrics.heightPixels;
        Glide.with(this).load(uri).asBitmap().centerCrop().error(mDefaultBackground)
                .into(new SimpleTarget<Bitmap>(width, height) {
                    @Override
                    public void onResourceReady(Bitmap resource,
                                                GlideAnimation<? super Bitmap> glideAnimation) {
                        if (mIsVideoSelected) mBackgroundManager.setBitmap(resource);
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        if (mIsVideoSelected) mBackgroundManager.setDrawable(errorDrawable);
                    }
                });
        killBackgroundTimer();
    }

    private void startBackgroundTimer() {
        killBackgroundTimer();
        mBackgroundTimer = new Timer();
        mBackgroundTimer.schedule(new UpdateBackgroundTask(), BACKGROUND_UPDATE_DELAY_MS);
    }

    private void killBackgroundTimer() {
        if (mBackgroundTimer != null) {
            mBackgroundTimer.cancel();
            mBackgroundTimer = null;
        }
    }

    private void startLiveCheckTimer() {
        killBackgroundTimer();
        mLiveCheckTimer = new Timer();
        mLiveCheckTimer.schedule(new LiveCheckTask(new LiveCheckTask.Callback() {
                    @Override
                    public void onCheckComplete(Video liveVideo) {
                        if (liveVideo != null) {
                            Log.i(TAG, "Live stream available!");
                            final ArrayObjectAdapter liveVideoRowAdapter =
                                        (ArrayObjectAdapter) getLiveVideoRow().getAdapter();
                            boolean videoAdded = false;
                            if (liveVideoRowAdapter.size() > 0) {
                                for (int i = 0; i < liveVideoRowAdapter.size(); i++) {
                                    Object rowItem = liveVideoRowAdapter.get(i);
                                    if (rowItem instanceof Video) {
                                        // Either the stream is already there, or we're about to
                                        // replace an existing stream with this one
                                        videoAdded = true;

                                        if (!liveVideo.equals(rowItem)) {
                                            // The new live video object is different from the previous
                                            // one, swap
                                            // the old one out for the new one.
                                            liveVideoRowAdapter.replace(i, liveVideo);
                                            videoAdded = true;
                                        }
                                    }
                                }
                            }
                            if (!videoAdded) {
                                // We haven't added the video yet, so add it
                                liveVideoRowAdapter.add(liveVideo);
                            }

                            if (mVideoTypeRowAdapter.indexOf(getLiveVideoRow()) == -1) {
                                mVideoTypeRowAdapter.add(0, getLiveVideoRow());
                            }
                        } else {
                            final ArrayObjectAdapter liveVideoRowAdapter =
                                    (ArrayObjectAdapter) getLiveVideoRow().getAdapter();
                            for (int i = 0; i < liveVideoRowAdapter.size(); i++) {
                                Object rowItem = liveVideoRowAdapter.get(i);
                                if (rowItem instanceof Video) {
                                    // Delete the stale livestream
                                    liveVideoRowAdapter.remove(rowItem);
                                }
                            }
                            if (liveVideoRowAdapter.size() < 1) {
                                mVideoTypeRowAdapter.remove(getLiveVideoRow());
                            }
                        }
                    }
                }, ApiKeyManager.getApiKey(getActivity()), getString(R.string
                        .default_live_stream_title)),
                0, LIVE_CHECK_INTERVAL_MS);
    }

    private void killLiveCheckTimer() {
        if (mLiveCheckTimer != null) {
            mLiveCheckTimer.cancel();
            mLiveCheckTimer = null;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        final String orderByDateTime =
                "datetime(" + VideoContract.VideoEntry.COLUMN_PUBLISH_DATE + ") DESC";
        if (id == VIDEO_TYPE_LOADER) {
            // Get all of the video types in the database.
            return new CursorLoader(getActivity(),
                    VideoContract.VideoEntry.TYPE_NAMES_SORTED_BY_MOST_RECENT_URI, null, null,
                    // No selection clause
                    null, // No selection arguments
                    null);
        } else if (id == CONTINUE_WATCHING_VIDEO_LOADER) {
            // Get all videos that have any progress saved, sorted by the most recently updated
            // saved position.
            final String posColName = VideoContract.VideoPosition.COLUMN_VIDEO_POSITION_MILLIS;
            final String selectionClause =
                    String.format("%s != ? AND %s > ? AND %s < %s * 1000", posColName, posColName,
                            posColName,
                            // This last part of the clause doesn't seem right but it doesn't
                            // seem to work
                            // if the length_seconds column + multiplier is in the selectionArgs
                            // array.
                            VideoContract.VideoEntry.COLUMN_LENGTH_SECONDS);
            final String[] selectionArgs = new String[] {"''", "0"};
            return new CursorLoader(getActivity(), VideoContract.VideoEntry.CONTENT_URI, null, //
                    // No projection (return all fields)
                    selectionClause, selectionArgs,
                    VideoContract.VideoPosition.COLUMN_LAST_UPDATED + " DESC, " + orderByDateTime);
        } else if (id == MOST_RECENT_VIDEO_LOADER) {
            // Get all videos, ordered by date.
            return new CursorLoader(getActivity(), VideoContract.VideoEntry.CONTENT_URI, null, //
                    // No projection (return all fields)
                    null, // No selection clause
                    null, // No selection arguments
                    orderByDateTime);
        } else {
            // Assume it is for a video type.
            final String videoTypeName = args.getString(VideoContract.VideoEntry.COLUMN_TYPE_NAME);

            // This just creates a CursorLoader that gets all videos for the given video type.
            return new CursorLoader(getActivity(), VideoContract.VideoEntry.CONTENT_URI, null, //
                    // No projection (return all fields)
                    VideoContract.VideoEntry.COLUMN_TYPE_NAME + " LIKE ?",
                    new String[] {"%" + videoTypeName + "%"},  // Select based on the video type
                    orderByDateTime);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.d(TAG, "onLoadFinished called with " + ((data == null) ? "" : "non-") + "null data " +
                "for loader ID " + loader.getId());
        if (data != null && !data.isClosed() && data.moveToFirst()) {
            final int loaderId = loader.getId();

            if (loaderId == VIDEO_TYPE_LOADER) {
                initContinueWatchingRow();
                initMostRecentRow();

                final Set<String> videoTypeNames = new LinkedHashSet<>();

                // Iterate through each video type and add it to the ArrayAdapter.
                while (!data.isAfterLast()) {
                    final int videoTypeIndex =
                            data.getColumnIndex(VideoContract.VideoEntry.COLUMN_TYPE_NAME);
                    final List<String> videoTypeNamesSplit =
                            Arrays.asList(data.getString(videoTypeIndex).split(","));

                    for (String videoTypeName : videoTypeNamesSplit) {
                        videoTypeName = videoTypeName.trim();
                        videoTypeNames.add(videoTypeName);
                    }

                    data.moveToNext();
                }

                for (String videoTypeName : videoTypeNames) {
                    // Create header for this video type.
                    final HeaderItem header = new HeaderItem(videoTypeName);

                    // Create unique int from video type.
                    final int videoLoaderId = videoTypeName.hashCode();
                    final CursorObjectAdapter existingAdapter =
                            mVideoCursorAdapters.get(videoLoaderId);
                    if (existingAdapter == null) {
                        // Map video results from the database to Video objects.
                        final CursorObjectAdapter videoCursorAdapter =
                                new CursorObjectAdapter(new VideoCardPresenter());
                        videoCursorAdapter.setMapper(
                                new VideoCursorMapper(ApiKeyManager.getApiKey(getActivity())));
                        mVideoCursorAdapters.put(videoLoaderId, videoCursorAdapter);

                        final ListRow row = new ListRow(header, videoCursorAdapter);

                        Log.d(TAG, "Loading videos for video type '" + videoTypeName + "' (" +
                                videoLoaderId + ")");
                        mVideoTypeRowAdapter.add(row);

                        // Start loading the videos from the database for a particular video
                        // type.
                        final Bundle args = new Bundle();
                        args.putString(VideoContract.VideoEntry.COLUMN_TYPE_NAME, videoTypeName);
                        getLoaderManager().initLoader(videoLoaderId, args, this);
                    }
                }

                initActionsRow();
                initLiveRow();

                // Map video type results from the database to ListRow objects.
                // This Adapter is used to render the MainFragment sidebar labels.
                // Do NOT call this any earlier otherwise when the app is restored after getting
                // killed by the system the adapter will not be sufficiently populated to avoid
                // an IndexOutOfBoundsException during layout because the ListRowDataAdapter
                // mLastVisibleRowIndex will be 0 but it will report a size of 1 when the adapter
                // actually has a size of 0.
                if (!mAdapterSet) {
                    setAdapter(mVideoTypeRowAdapter);
                    // Work around bug in earlier versions of the Leanback library in which
                    // setting the adapter a second time hits an IllegalArgumentException:
                    // Pre https://android.googlesource
                    // .com/platform/frameworks/support/+/1a7f5e94fb0f7183151be193ff0106ea19b65749
                    mAdapterSet = true;
                }

                // TODO: Move startEntranceTransition to after all cursors have loaded.
                startEntranceTransition();
            } else {
                if (loader.getId() == CONTINUE_WATCHING_VIDEO_LOADER &&
                        mContinueWatchingRow != null &&
                        mVideoTypeRowAdapter.indexOf(mContinueWatchingRow) < 0) {
                    // Ensure the 'continue watching' row is showing since it may have previously
                    // been hidden when it was empty.
                    mVideoTypeRowAdapter.add(0, mContinueWatchingRow);
                }
                // The CursorAdapter contains a Cursor pointing to videos.
                mVideoCursorAdapters.get(loaderId).changeCursor(data);
            }
        } else {
            Log.d(TAG, "Data is null or unable to move to first");
            if (loader.getId() == VIDEO_TYPE_LOADER && mVideoCursorAdapters.size() == 0 &&
                    LastRefreshTimePrefUtils.shouldRefresh(getActivity(), false)) {
                Log.d(TAG, "No data loaded yet and shouldRefresh is true, starting a fetch");
                beginVideoFetch(false);
            } else if (loader.getId() == CONTINUE_WATCHING_VIDEO_LOADER &&
                    mContinueWatchingRow != null) {
                // Hide the 'continue watching' row because we don't have any videos in progress.
                mVideoTypeRowAdapter.remove(mContinueWatchingRow);
            }
        }
    }

    private void initContinueWatchingRow() {
        if (getLoaderManager().getLoader(CONTINUE_WATCHING_VIDEO_LOADER) != null) return;

        // Create a special 'continue watching' row.
        final CursorObjectAdapter continueWatchingCursorAdapter =
                new CursorObjectAdapter(new VideoCardPresenter(CardPresenter.CardSize.LARGE));
        continueWatchingCursorAdapter
                .setMapper(new VideoCursorMapper(ApiKeyManager.getApiKey(getActivity())));
        mVideoCursorAdapters.put(CONTINUE_WATCHING_VIDEO_LOADER, continueWatchingCursorAdapter);

        final String continueWatchingName =
                getResources().getString(R.string.continue_watching_videos_header);
        final HeaderItem continueWatchingHeader = new HeaderItem(continueWatchingName);
        mContinueWatchingRow = new ListRow(continueWatchingHeader, continueWatchingCursorAdapter);
        mVideoTypeRowAdapter.add(mContinueWatchingRow);

        getLoaderManager().initLoader(CONTINUE_WATCHING_VIDEO_LOADER, null, this);
    }

    private void initMostRecentRow() {
        if (getLoaderManager().getLoader(MOST_RECENT_VIDEO_LOADER) != null) return;

        // Create a special 'most recent' row.
        final CursorObjectAdapter mostRecentCursorAdapter =
                new CursorObjectAdapter(new VideoCardPresenter(CardPresenter.CardSize.LARGE));
        mostRecentCursorAdapter
                .setMapper(new VideoCursorMapper(ApiKeyManager.getApiKey(getActivity())));
        mVideoCursorAdapters.put(MOST_RECENT_VIDEO_LOADER, mostRecentCursorAdapter);

        final String mostRecentName = getResources().getString(R.string.most_recent_videos_header);
        final HeaderItem mostRecentHeader = new HeaderItem(mostRecentName);
        final ListRow mostRecentRow = new ListRow(mostRecentHeader, mostRecentCursorAdapter);
        mVideoTypeRowAdapter.add(mostRecentRow);
        getLoaderManager().initLoader(MOST_RECENT_VIDEO_LOADER, null, this);
    }

    private void initActionsRow() {
        if (mActionsRow != null) {
            mVideoTypeRowAdapter.remove(mActionsRow);
        } else {
            // Create a row at the bottom for non-video actions.
            final ArrayObjectAdapter actionsRowAdapter =
                    new ArrayObjectAdapter(new DrawableImageCardPresenter());
            final DrawableGridItem refreshGridItem =
                    new DrawableGridItem(REFRESH_ITEM_ID, getString(R.string.refresh),
                            R.drawable.ic_refresh);
            actionsRowAdapter.add(refreshGridItem);

            final DrawableGridItem selectDefaultQualityItem =
                    new DrawableGridItem(SELECT_DEFAULT_QUALITY_ITEM_ID,
                            getString(R.string.select_default_quality),
                            R.drawable.ic_video_quality);
            actionsRowAdapter.add(selectDefaultQualityItem);

            final DrawableGridItem clearAuthGridItem = new DrawableGridItem(CLEAR_AUTH_ITEM_ID,
                    getString(R.string.clear_authorization), R.drawable.ic_unlock);
            actionsRowAdapter.add(clearAuthGridItem);

            final DrawableGridItem showAppInfoGridItem =
                    new DrawableGridItem(SHOW_APP_INFO_ITEM_ID, getString(R.string.show_app_info),
                            R.drawable.ic_about);
            actionsRowAdapter.add(showAppInfoGridItem);

            final HeaderItem actionsHeader = new HeaderItem(getString(R.string.actions_header));
            mActionsRow = new ListRow(actionsHeader, actionsRowAdapter);
        }
        mVideoTypeRowAdapter.add(mActionsRow);
    }

    private void initLiveRow() {
        final String apiKey = ApiKeyManager.getApiKey(getActivity());
        final ApiGetRequestParams params = new ApiGetRequestParams(apiKey, "chats");
        final ApiGetRequestTask task = new ApiGetRequestTask() {
            @Override
            protected void onPostExecute(Map<ApiGetRequestParams, JSONObject> responses) {
                final JSONObject responseJson = responses.get(params);
                if (responseJson == null) {
                    Log.d(TAG, "Chats Response JSON was null");
                    return;
                }

                final JSONArray resultsArray = responseJson.optJSONArray("results");
                if (resultsArray == null) {
                    Log.d(TAG, "Unable to find results array in results JSON, returning");
                }

                for (int i = 0; i < resultsArray.length(); i++) {
                    try {
                        JSONObject resultsObj = resultsArray.getJSONObject(i);
                        if (!resultsObj.isNull("channel_name")) {
                            String channelName = resultsObj.getString("channel_name");
                            if (channelName.contains("giantbomb")) {
                                //Assume it's twitch
                                mInfiniteUrl = Uri.parse("twitch://stream/" + channelName);
                                mInfiniteOnYoutube = false;
                            } else {
                                //Assume it's YouTube
                                mInfiniteOnYoutube = true;
                                if (Utils.isAmazonDevice()) {
                                    mInfiniteUrl = Uri.parse("https://www.youtube.com/tv#/watch/video/control?v=" + channelName);
                                } else {
                                    mInfiniteUrl = Uri.parse("https://www.youtube.com/watch?v=" + channelName);

                                }
                            }

                            ArrayObjectAdapter adapter = (ArrayObjectAdapter) getLiveVideoRow().getAdapter();
                            //Check if we already have a live item
                            boolean exists = false;
                            for (int j = 0; j < adapter.size(); j++) {
                                Object item = adapter.get(j);
                                if (item instanceof DrawableGridItem && ((DrawableGridItem) item).getId() == INFINITE_ITEM_ID) {
                                    exists = true;
                                }

                            }
                            if (!exists) {
                                final DrawableGridItem infiniteGridItem =
                                        new DrawableGridItem(INFINITE_ITEM_ID,
                                                getString(R.string.infinite_title), R.drawable.ic_infinite);
                                adapter.add(infiniteGridItem);
                            }

                            if (adapter.size() > 0 && mVideoTypeRowAdapter.indexOf(getLiveVideoRow()) == -1) {
                                mVideoTypeRowAdapter.add(0, getLiveVideoRow());
                            }
                            return;
                        } else {
                            Log.d(TAG, "Channel name JSON string is null");
                        }
                    } catch (JSONException e) {
                        // Fall through to return null.
                        Log.d(TAG, "Unable to get results object or channel name from JSON response", e);
                    }
                }
            }

        };
        task.execute(params);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        int loaderId = loader.getId();
        if (loaderId != VIDEO_TYPE_LOADER) {
            mVideoCursorAdapters.get(loaderId).changeCursor(null);
        }
    }

    /**
     * Launches the video details activity for the provided video.
     *
     * @param video              The video to start the details activity for.
     * @param imageCardView      The image card view to use in the scene transition. If this is
     *                           null,
     *                           then no scene transition will occur.
     * @param startVideoDirectly If true, the details activity will immediately launch into the
     *                           playback activity and skip quality selection.
     */
    private void startDetailsActivity(Video video, @Nullable ImageCardView imageCardView,
                                      boolean startVideoDirectly) {
        final Intent intent = new Intent(getActivity(), VideoDetailsActivity.class);
        intent.putExtra(VideoDetailsActivity.VIDEO, video);
        intent.putExtra(VideoDetailsActivity.START_VIDEO_DIRECTLY, startVideoDirectly);

        if (Utils.isLowEndDevice() || imageCardView == null) {
            getActivity().startActivity(intent);
        } else {
            final Bundle bundle = ActivityOptionsCompat
                    .makeSceneTransitionAnimation(getActivity(), imageCardView.getMainImageView(),
                            VideoDetailsActivity.SHARED_ELEMENT_NAME).toBundle();
            getActivity().startActivity(intent, bundle);
        }
    }

    private void clearAuthorization() {
        if (ApiKeyManager.deleteApiKey(getActivity())) {
            addSpinner();
            new AsyncTask<Void, Void, Void>() {
                @Override
                public Void doInBackground(Void... unused) {
                    final VideoDbHelper helper = new VideoDbHelper(getActivity());
                    helper.clearDatabase();
                    DefaultQualityPrefUtils.reset(getActivity());
                    return null;
                }

                @Override
                public void onPostExecute(Void unused) {
                    final Intent intent = new Intent(getActivity(), LauncherActivity.class);
                    intent.setFlags(
                            Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                    getActivity().startActivity(intent);
                    getActivity().finish();
                }
            }.execute();
        } else {
            Utils.showToast(getActivity(), R.string.clear_authorization_error);
        }
    }

    private ListRow getLiveVideoRow() {
        if (mLiveVideoRow == null) {
            final ArrayObjectAdapter liveVideoRowAdapter = new
                    ArrayObjectAdapter(
                    new VideoCardPresenter(CardPresenter.CardSize.LARGE));
            final HeaderItem liveVideoHeader =
                    new IconHeaderItem(getString(R.string.live_video_header),
                            R.drawable.ic_record);
            mLiveVideoRow = new ListRow(liveVideoHeader, liveVideoRowAdapter);
        }
        return mLiveVideoRow;
    }

    private class UpdateBackgroundTask extends TimerTask {
        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (mBackgroundUri != null) {
                        updateBackground(mBackgroundUri.toString());
                    }
                }
            });
        }
    }

    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item,
                                  RowPresenter.ViewHolder rowViewHolder, Row row) {
            if (item instanceof Video) {
                startDetailsActivity((Video) item, (ImageCardView) itemViewHolder.view, false);
            } else if (item instanceof DrawableGridItem) {
                final DrawableGridItem actionItem = (DrawableGridItem) item;
                if (actionItem.getId() == REFRESH_ITEM_ID) {
                    refreshVideos(true, false);
                } else if (actionItem.getId() == SELECT_DEFAULT_QUALITY_ITEM_ID) {
                    final DialogFragment selectQualityDialog =
                            new SelectDefaultQualityDialogFragment();
                    selectQualityDialog.show(getFragmentManager(), "selectDefaultQuality");
                } else if (actionItem.getId() == CLEAR_AUTH_ITEM_ID) {
                    final ClearAuthorizationDialogFragment clearAuthDialogFragment =
                            new ClearAuthorizationDialogFragment();
                    clearAuthDialogFragment.setOnClearButtonCallback(
                            new ClearAuthorizationDialogFragment.ClearAuthCallback() {
                                @Override
                                public void onClearButton() {
                                    clearAuthorization();
                                }
                            });
                    clearAuthDialogFragment.show(getFragmentManager(), "clearAuth");
                } else if (actionItem.getId() == SHOW_APP_INFO_ITEM_ID) {
                    final DialogFragment appInfoDialogFragment = new AppInfoDialogFragment();
                    appInfoDialogFragment.show(getFragmentManager(), "appInfo");
                } else if (actionItem.getId() == INFINITE_ITEM_ID) {
                    try {
                        Intent infinitePlay = new Intent(Intent.ACTION_VIEW, mInfiniteUrl);
                        startActivity(infinitePlay);
                    } catch (ActivityNotFoundException e) {
                        final ErrorFragment errorFragment = new ErrorFragment();
                        String errorMessage = getString(R.string.infinite_tw_error_message);
                        if (mInfiniteOnYoutube) {
                            errorMessage = getString(R.string.infinite_yt_error_message);
                        }
                        errorFragment.setMessage(errorMessage);
                        errorFragment.setButtonText(getString(R.string.dismiss));
                        errorFragment.setImageDrawable(getResources().getDrawable(R.drawable.lb_ic_sad_cloud));
                        errorFragment.setButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                getFragmentManager().beginTransaction().remove(errorFragment).commit();
                            }
                        });
                        getFragmentManager().beginTransaction()
                                .replace(R.id.main_browse_fragment, errorFragment)
                                .addToBackStack(null)
                                .commit();
                    }
                }
            }
        }
    }

    private final class ItemViewSelectedListener implements OnItemViewSelectedListener {
        @Override
        public void onItemSelected(Presenter.ViewHolder itemViewHolder, Object item,
                                   RowPresenter.ViewHolder rowViewHolder, Row row) {
            if (item instanceof Video) {
                mIsVideoSelected = true;
                final Video video = (Video) item;

                mBackgroundUri = video.getSuperImageUrl() != null ?
                        Uri.parse(((Video) item).getSuperImageUrl()) : null;
                startBackgroundTimer();

                mSelectedVideo = video;
            } else {
                mIsVideoSelected = false;
                killBackgroundTimer();
                mBackgroundManager.setDrawable(
                        getResources().getDrawable(R.drawable.default_background, null));

                mSelectedVideo = null;
            }
        }
    }
}
