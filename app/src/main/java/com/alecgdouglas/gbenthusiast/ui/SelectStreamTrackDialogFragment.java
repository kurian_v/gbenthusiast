package com.alecgdouglas.gbenthusiast.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ArrayAdapter;

import com.alecgdouglas.gbenthusiast.R;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.RendererCapabilities;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.FixedTrackSelection;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.util.MimeTypes;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * Well, gosh. This is a very confusing class that is based off of the TrackSelectorHelper class
 * in the ExoPlayer demo app. It's really only intended to be used for live streams but parts are
 * general enough for non-live streams. At the time of writing, I really only understand ~60% of
 * what's done in here. Good luck!
 */
public class SelectStreamTrackDialogFragment extends DialogFragment {
    private static final TrackSelection.Factory FIXED_FACTORY = new FixedTrackSelection.Factory();
    private final List<TrackOption> mTrackOptions = new ArrayList<>();
    private boolean mInitialized = false;
    private MappingTrackSelector mTrackSelector;
    private TrackSelection.Factory mTrackSelectionFactory;
    private MappingTrackSelector.MappedTrackInfo mCurrentTrackInfo;
    private int mRendererIndex;
    private TrackOption mDefaultTrackOption;

    private TrackGroupArray mTrackGroups;
    private boolean[] mTrackGroupsAdaptive;
    private MappingTrackSelector.SelectionOverride mOverride;

    private static String buildTrackName(Format format, String unknownTrackName) {
        String trackName;
        if (MimeTypes.isVideo(format.sampleMimeType)) {
            trackName = joinWithSeparator(
                    joinWithSeparator(buildResolutionString(format), buildBitrateString(format)),
                    buildTrackIdString(format));
        } else if (MimeTypes.isAudio(format.sampleMimeType)) {
            trackName = joinWithSeparator(joinWithSeparator(
                    joinWithSeparator(buildLanguageString(format),
                            buildAudioPropertyString(format)), buildBitrateString(format)),
                    buildTrackIdString(format));
        } else {
            trackName = joinWithSeparator(
                    joinWithSeparator(buildLanguageString(format), buildBitrateString(format)),
                    buildTrackIdString(format));
        }
        return trackName.length() == 0 ? unknownTrackName : trackName;

    }

    private static String buildResolutionString(Format format) {
        return format.width == Format.NO_VALUE || format.height == Format.NO_VALUE ? "" :
                format.width + "x" + format.height;
    }

    private static String buildAudioPropertyString(Format format) {
        return format.channelCount == Format.NO_VALUE || format.sampleRate == Format.NO_VALUE ? "" :
                format.channelCount + "ch, " + format.sampleRate + "Hz";
    }

    private static String buildLanguageString(Format format) {
        return TextUtils.isEmpty(format.language) || "und".equals(format.language) ? "" :
                format.language;
    }

    private static String buildBitrateString(Format format) {
        return format.bitrate == Format.NO_VALUE ? "" :
                String.format(Locale.US, "%.2fMbit", format.bitrate / 1000000f);
    }

    private static String joinWithSeparator(String first, String second) {
        return first.length() == 0 ? second :
                (second.length() == 0 ? first : first + ", " + second);
    }

    private static String buildTrackIdString(Format format) {
        return format.id == null ? "" : ("id:" + format.id);
    }

    public void init(MappingTrackSelector selector, TrackSelection.Factory trackSelectionFactory) {
        mTrackSelector = selector;
        mTrackSelectionFactory = trackSelectionFactory;
        mInitialized = true;
    }

    public void release() {
        mTrackSelector = null;
        mTrackSelectionFactory = null;
        mCurrentTrackInfo = null;
        mTrackOptions.clear();
        mDefaultTrackOption = null;
        mTrackGroups = null;
        mTrackGroupsAdaptive = null;
        mOverride = null;
        mInitialized = false;
    }

    public void show(FragmentManager fragmentManager, String tag,
                     MappingTrackSelector.MappedTrackInfo currentTrackInfo, int rendererIndex,
                     Resources resources) {
        if (!mInitialized) return;

        mCurrentTrackInfo = currentTrackInfo;
        mRendererIndex = rendererIndex;

        mTrackGroups = mCurrentTrackInfo.getTrackGroups(rendererIndex);
        mTrackGroupsAdaptive = new boolean[mTrackGroups.length];
        for (int i = 0; i < mTrackGroups.length; i++) {
            mTrackGroupsAdaptive[i] =
                    (mTrackSelectionFactory instanceof AdaptiveVideoTrackSelection.Factory) &&
                            (mCurrentTrackInfo.getAdaptiveSupport(rendererIndex, i, false) !=
                                    RendererCapabilities.ADAPTIVE_NOT_SUPPORTED) &&
                            (mTrackGroups.get(i).length > 1);
        }
        mOverride = mTrackSelector.getSelectionOverride(rendererIndex, mTrackGroups);

        mTrackOptions.clear();
        for (int groupIndex = 0; groupIndex < mTrackGroups.length; groupIndex++) {
            final TrackGroup group = mTrackGroups.get(groupIndex);
            for (int trackIndex = 0; trackIndex < group.length; trackIndex++) {
                if (mCurrentTrackInfo
                        .getTrackFormatSupport(rendererIndex, groupIndex, trackIndex) ==
                        RendererCapabilities.FORMAT_HANDLED) {
                    final String trackName = buildTrackName(group.getFormat(trackIndex),
                            resources.getString(R.string.unknown));
                    final TrackOption trackOption =
                            new TrackOption(trackName, groupIndex, trackIndex);
                    mTrackOptions.add(trackOption);
                }
            }
        }

        final int defaultOptionTitleResId =
                mTrackOptions.isEmpty() ? R.string.selection_default_none :
                        R.string.selection_default;
        mDefaultTrackOption = new TrackOption(resources.getString(defaultOptionTitleResId), -1, -1);
        mTrackOptions.add(0, mDefaultTrackOption);

        show(fragmentManager, tag);
    }

    @Override
    public void show(FragmentManager fragmentManager, String tag) {
        if (mTrackOptions.isEmpty()) return;
        super.show(fragmentManager, tag);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.live_selection_dialog_title)
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        final List<String> trackNames = new ArrayList<>(mTrackOptions.size());
        for (TrackOption trackOption : mTrackOptions) {
            trackNames.add(trackOption.getName());
        }

        final ArrayAdapter<String> trackNamesAdapter =
                new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_singlechoice);
        trackNamesAdapter.addAll(trackNames);

        final TrackOption selectedOption = getSelectedOption();
        final int selectedNamePosition =
                Math.max(trackNamesAdapter.getPosition(selectedOption.getName()), 0);

        builder.setSingleChoiceItems(trackNamesAdapter, selectedNamePosition,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which != selectedNamePosition) {
                            final TrackOption selectedTrackOption = mTrackOptions.get(which);
                            setSelectedTrack(selectedTrackOption);
                        }
                        dialog.dismiss();
                    }
                });
        final AlertDialog alertDialog = builder.create();
        return alertDialog;
    }

    private void setSelectedTrack(TrackOption selectedTrackOption) {
        if (selectedTrackOption.equals(mDefaultTrackOption)) {
            mOverride = null;
        } else {
            final int groupIndex = selectedTrackOption.getGroupIndex();
            final int trackIndex = selectedTrackOption.getTrackIndex();

            if (!mTrackGroupsAdaptive[groupIndex] || mOverride == null ||
                    mOverride.groupIndex != groupIndex) {
                mOverride = new MappingTrackSelector.SelectionOverride(FIXED_FACTORY, groupIndex,
                        trackIndex);
            } else {
                setOverride(groupIndex, new int[] {trackIndex});
            }
        }

        if (mOverride != null) {
            mTrackSelector.setSelectionOverride(mRendererIndex, mTrackGroups, mOverride);
        } else {
            mTrackSelector.clearSelectionOverrides(mRendererIndex);
        }
    }

    private void setOverride(int group, int[] tracks) {
        final TrackSelection.Factory factory =
                (tracks.length == 1) ? FIXED_FACTORY : mTrackSelectionFactory;
        mOverride = new MappingTrackSelector.SelectionOverride(factory, group, tracks);
    }

    private TrackOption getSelectedOption() {
        if (mOverride == null) {
            return mDefaultTrackOption;
        } else {
            for (TrackOption trackOption : mTrackOptions) {
                if (mOverride.groupIndex == trackOption.getGroupIndex() &&
                        mOverride.containsTrack(trackOption.getTrackIndex())) {
                    return trackOption;
                }
            }
        }
        return null;
    }

    private static class TrackOption {
        final String mName;
        final int mGroupIndex;
        final int mTrackIndex;

        public TrackOption(String name, int groupIndex, int trackIndex) {
            mName = name;
            mGroupIndex = groupIndex;
            mTrackIndex = trackIndex;
        }

        public String getName() {
            return mName;
        }

        public int getGroupIndex() {
            return mGroupIndex;
        }

        public int getTrackIndex() {
            return mTrackIndex;
        }

        @Override
        public boolean equals(Object other) {
            if (this == other) return true;
            if (other == null) return false;
            if (getClass() != other.getClass()) return false;

            final TrackOption option = (TrackOption) other;

            return Objects.equals(mName, option.mName) && mGroupIndex == mGroupIndex &&
                    mTrackIndex == option.mTrackIndex;
        }

        @Override
        public int hashCode() {
            return Objects.hash(mName, mGroupIndex, mTrackIndex);
        }
    }
}
