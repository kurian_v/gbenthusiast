package com.alecgdouglas.gbenthusiast;

import android.os.Handler;
import android.os.SystemClock;

import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.SlidingPercentile;

import java.util.concurrent.TimeUnit;

/**
 * Fork of DefaultBandwidthMeter that resets the estimate when it gets stuck on a bitrate and stops
 * receiving updates.
 */
public class ResettingBandwidthMeter implements BandwidthMeter, TransferListener<Object> {
    /**
     * The default maximum weight for the sliding window.
     */
    public static final int DEFAULT_MAX_WEIGHT = 2000;
    private static final long ESTIMATE_UPDATE_RESET_TIMEOUT_MILLIS = TimeUnit.SECONDS.toMillis(15L);
    private static final long MIN_RESET_SAMPLE_TIME_MS = 3000L;
    private static final int ELAPSED_MILLIS_FOR_ESTIMATE = 2000;
    private static final int BYTES_TRANSFERRED_FOR_ESTIMATE = 512 * 1024;

    private final Handler eventHandler;
    private final EventListener eventListener;
    private final SlidingPercentile slidingPercentile;

    private int streamCount;
    private long sampleStartTimeMs;
    private long sampleBytesTransferred;

    private long totalElapsedTimeMs;
    private long totalBytesTransferred;
    private long bitrateEstimate;

    private long mMillisSinceLastNonZeroSampleBytes = 0L;

    public ResettingBandwidthMeter() {
        this(null, null);
    }

    public ResettingBandwidthMeter(Handler eventHandler, EventListener eventListener) {
        this(eventHandler, eventListener, DEFAULT_MAX_WEIGHT);
    }

    public ResettingBandwidthMeter(Handler eventHandler, EventListener eventListener,
                                   int maxWeight) {
        this.eventHandler = eventHandler;
        this.eventListener = eventListener;
        this.slidingPercentile = new SlidingPercentile(maxWeight);
        bitrateEstimate = NO_ESTIMATE;
    }

    @Override
    public synchronized long getBitrateEstimate() {
        return bitrateEstimate;
    }

    @Override
    public synchronized void onTransferStart(Object source, DataSpec dataSpec) {
        if (streamCount == 0) {
            sampleStartTimeMs = SystemClock.elapsedRealtime();
        }
        streamCount++;
    }

    @Override
    public synchronized void onBytesTransferred(Object source, int bytes) {
        sampleBytesTransferred += bytes;
    }

    @Override
    public synchronized void onTransferEnd(Object source) {
        Assertions.checkState(streamCount > 0);
        long nowMs = SystemClock.elapsedRealtime();
        int sampleElapsedTimeMs = (int) (nowMs - sampleStartTimeMs);
        if (sampleBytesTransferred == 0) {
            mMillisSinceLastNonZeroSampleBytes += sampleElapsedTimeMs;
        } else {
            mMillisSinceLastNonZeroSampleBytes = 0L;
        }
        totalElapsedTimeMs += sampleElapsedTimeMs;
        totalBytesTransferred += sampleBytesTransferred;
        if (sampleElapsedTimeMs > 0) {
            float bitsPerSecond = (sampleBytesTransferred * 8000) / sampleElapsedTimeMs;
            slidingPercentile.addSample((int) Math.sqrt(sampleBytesTransferred), bitsPerSecond);
            if (totalElapsedTimeMs >= ELAPSED_MILLIS_FOR_ESTIMATE ||
                    totalBytesTransferred >= BYTES_TRANSFERRED_FOR_ESTIMATE) {
                float bitrateEstimateFloat = slidingPercentile.getPercentile(0.5f);
                bitrateEstimate = Float.isNaN(bitrateEstimateFloat) ? NO_ESTIMATE :
                        (long) bitrateEstimateFloat;
                if (mMillisSinceLastNonZeroSampleBytes > MIN_RESET_SAMPLE_TIME_MS) {
                    bitrateEstimate = NO_ESTIMATE;
                }
            }
        }
        notifyBandwidthSample(sampleElapsedTimeMs, sampleBytesTransferred, bitrateEstimate);
        if (--streamCount > 0) {
            sampleStartTimeMs = nowMs;
        }
        sampleBytesTransferred = 0;
    }

    private void notifyBandwidthSample(final int elapsedMs, final long bytes, final long bitrate) {
        if (eventHandler != null && eventListener != null) {
            eventHandler.post(new Runnable() {
                @Override
                public void run() {
                    eventListener.onBandwidthSample(elapsedMs, bytes, bitrate);
                }
            });
        }
    }
}
