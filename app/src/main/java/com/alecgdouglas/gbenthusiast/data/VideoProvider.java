/*
 * Copyright (c) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.alecgdouglas.gbenthusiast.data;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.alecgdouglas.gbenthusiast.data.VideoContract.VideoEntry;
import com.alecgdouglas.gbenthusiast.data.VideoContract.VideoPosition;

import java.util.HashMap;

/**
 * VideoProvider is a ContentProvider that provides videos for the rest of applications.
 */
public class VideoProvider extends ContentProvider {
    // These codes are returned from sUriMatcher#match when the respective Uri matches.
    private static final int VIDEO = 1;
    private static final int VIDEO_WITH_CATEGORY = 2;
    private static final int SEARCH_SUGGEST = 3;
    private static final int REFRESH_SHORTCUT = 4;
    private static final int VIDEO_POSITION = 5;
    private static final int TYPE_NAMES_SORTED_BY_MOST_RECENT = 6;
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private static final SQLiteQueryBuilder sVideosContainingQueryBuilder;
    private static final SQLiteQueryBuilder sTypeNamesSortedByMostRecentQueryBuilder;
    private static final SQLiteQueryBuilder sVideosWithPositionsQueryBuilder;
    private static final String[] sVideosContainingQueryColumns;
    private static final String[] sTypeNamesSortedByMostRecentQueryColumns;
    private static final HashMap<String, String> sColumnMap = buildColumnMap();

    static {
        sVideosContainingQueryBuilder = new SQLiteQueryBuilder();
        sVideosContainingQueryBuilder.setTables(VideoEntry.TABLE_NAME);
        sVideosContainingQueryBuilder.setProjectionMap(sColumnMap);
        sVideosContainingQueryColumns =
                new String[] {VideoEntry._ID, VideoEntry.COLUMN_GIANT_BOMB_ID,
                        VideoEntry.COLUMN_NAME, VideoEntry.COLUMN_DECK, VideoEntry.COLUMN_TYPE_NAME,
                        VideoEntry.COLUMN_PUBLISH_DATE, VideoEntry.COLUMN_LENGTH_SECONDS,
                        VideoEntry.COLUMN_VIDEO_URL_HD, VideoEntry.COLUMN_VIDEO_URL_HIGH,
                        VideoEntry.COLUMN_VIDEO_URL_LOW, VideoEntry.COLUMN_IMAGE_URL_TINY,
                        VideoEntry.COLUMN_IMAGE_URL_MEDIUM, VideoEntry.COLUMN_IMAGE_URL_SUPER,
                        VideoEntry.COLUMN_IMAGE_URL_ICON};

        sTypeNamesSortedByMostRecentQueryBuilder = new SQLiteQueryBuilder();
        sTypeNamesSortedByMostRecentQueryBuilder.setTables(VideoEntry.TABLE_NAME);
        sTypeNamesSortedByMostRecentQueryBuilder.setProjectionMap(sColumnMap);
        sTypeNamesSortedByMostRecentQueryColumns =
                new String[] {VideoEntry.COLUMN_TYPE_NAME, VideoEntry.COLUMN_PUBLISH_DATE};

        sVideosWithPositionsQueryBuilder = new SQLiteQueryBuilder();
        final String tables =
                String.format("%s LEFT OUTER JOIN %s ON %s.%s = %s.%s", VideoEntry.TABLE_NAME,
                        VideoPosition.TABLE_NAME, VideoEntry.TABLE_NAME,
                        VideoEntry.COLUMN_GIANT_BOMB_ID, VideoPosition.TABLE_NAME,
                        VideoPosition.COLUMN_VIDEO_ID);
        sVideosWithPositionsQueryBuilder.setTables(tables);
    }

    private VideoDbHelper mOpenHelper;
    private ContentResolver mContentResolver;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = VideoContract.CONTENT_AUTHORITY;

        // For each type of URI to add, create a corresponding code.
        matcher.addURI(authority, VideoContract.PATH_VIDEO, VIDEO);
        matcher.addURI(authority, VideoContract.PATH_VIDEO + "/*", VIDEO_WITH_CATEGORY);

        matcher.addURI(authority, VideoContract.PATH_VIDEO_POSITION, VIDEO_POSITION);

        // Search related URIs.
        matcher.addURI(authority, SearchManager.SUGGEST_URI_PATH_QUERY, SEARCH_SUGGEST);
        matcher.addURI(authority, SearchManager.SUGGEST_URI_PATH_QUERY + "/*", SEARCH_SUGGEST);

        matcher.addURI(authority, VideoContract.PATH_TYPE_NAMES_SORTED_BY_MOST_RECENT,
                TYPE_NAMES_SORTED_BY_MOST_RECENT);

        return matcher;
    }

    private static HashMap<String, String> buildColumnMap() {
        final HashMap<String, String> map = new HashMap<>();
        map.put(VideoEntry.COLUMN_GIANT_BOMB_ID, VideoEntry.COLUMN_GIANT_BOMB_ID);
        map.put(VideoEntry.COLUMN_NAME, VideoEntry.COLUMN_NAME);
        map.put(VideoEntry.COLUMN_DECK, VideoEntry.COLUMN_DECK);
        map.put(VideoEntry.COLUMN_TYPE_NAME, VideoEntry.COLUMN_TYPE_NAME);
        map.put(VideoEntry.COLUMN_PUBLISH_DATE, VideoEntry.COLUMN_PUBLISH_DATE);
        map.put(VideoEntry.COLUMN_LENGTH_SECONDS, VideoEntry.COLUMN_LENGTH_SECONDS);
        map.put(VideoEntry.COLUMN_VIDEO_URL_HD, VideoEntry.COLUMN_VIDEO_URL_HD);
        map.put(VideoEntry.COLUMN_VIDEO_URL_HIGH, VideoEntry.COLUMN_VIDEO_URL_HIGH);
        map.put(VideoEntry.COLUMN_VIDEO_URL_LOW, VideoEntry.COLUMN_VIDEO_URL_LOW);
        map.put(VideoEntry.COLUMN_IMAGE_URL_TINY, VideoEntry.COLUMN_IMAGE_URL_TINY);
        map.put(VideoEntry.COLUMN_IMAGE_URL_MEDIUM, VideoEntry.COLUMN_IMAGE_URL_MEDIUM);
        map.put(VideoEntry.COLUMN_IMAGE_URL_SUPER, VideoEntry.COLUMN_IMAGE_URL_SUPER);
        map.put(VideoEntry.COLUMN_IMAGE_URL_ICON, VideoEntry.COLUMN_IMAGE_URL_ICON);
        return map;
    }

    @Override
    public boolean onCreate() {
        final Context context = getContext();
        mContentResolver = context.getContentResolver();
        mOpenHelper = new VideoDbHelper(context);
        return true;
    }

    private Cursor getSuggestions(String query) {
        query = query.toLowerCase();
        return sVideosContainingQueryBuilder
                .query(mOpenHelper.getReadableDatabase(), sVideosContainingQueryColumns,
                        VideoEntry.COLUMN_NAME + " LIKE ? OR " + VideoEntry.COLUMN_DECK + " LIKE ?",
                        new String[] {"%" + query + "%", "%" + query + "%"}, null, null, null);
    }

    private Cursor getTypeNamesSortedByMostRecent() {
        return sTypeNamesSortedByMostRecentQueryBuilder
                .query(mOpenHelper.getReadableDatabase(), sTypeNamesSortedByMostRecentQueryColumns,
                        null, null, VideoEntry.COLUMN_TYPE_NAME, null,
                        "MAX(datetime(" + VideoEntry.COLUMN_PUBLISH_DATE + ")) DESC");
    }

    private Cursor getVideosWithPositions(String[] projection, String selection,
                                          String[] selectionArgs, String sortOrder) {
        return sVideosWithPositionsQueryBuilder
                .query(mOpenHelper.getReadableDatabase(), projection, selection, selectionArgs,
                        null, // no group-by
                        null, // no having
                        sortOrder);
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Cursor retCursor;
        switch (sUriMatcher.match(uri)) {
            case SEARCH_SUGGEST: {
                String rawQuery = "";
                if (selectionArgs != null && selectionArgs.length > 0) {
                    rawQuery = selectionArgs[0];
                }
                retCursor = getSuggestions(rawQuery);
                break;
            }
            case VIDEO: {
                retCursor = getVideosWithPositions(projection, selection, selectionArgs, sortOrder);
                break;
            }
            case VIDEO_POSITION: {
                retCursor = mOpenHelper.getReadableDatabase()
                        .query(VideoPosition.TABLE_NAME, projection, selection, selectionArgs, null,
                                null, sortOrder);
                break;
            }
            case TYPE_NAMES_SORTED_BY_MOST_RECENT: {
                retCursor = getTypeNamesSortedByMostRecent();
                break;
            }
            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }

        retCursor.setNotificationUri(mContentResolver, uri);
        return retCursor;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        switch (sUriMatcher.match(uri)) {
            // The application is querying the db for its own contents.
            case VIDEO_WITH_CATEGORY:
                return VideoEntry.CONTENT_TYPE;
            case VIDEO:
                return VideoEntry.CONTENT_TYPE;
            case VIDEO_POSITION:
                return VideoPosition.CONTENT_TYPE;

            // The Android TV global search is querying our app for relevant content.
            case SEARCH_SUGGEST:
                return SearchManager.SUGGEST_MIME_TYPE;
            case REFRESH_SHORTCUT:
                return SearchManager.SHORTCUT_MIME_TYPE;

            // We aren't sure what is being asked of us.
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        final Uri returnUri;
        final int match = sUriMatcher.match(uri);

        switch (match) {
            case VIDEO: {
                long _id = mOpenHelper.getWritableDatabase()
                        .insert(VideoEntry.TABLE_NAME, null, values);
                if (_id > 0) {
                    returnUri = VideoEntry.buildVideoUri(_id);
                } else {
                    throw new SQLException("Failed to insert row into " + uri);
                }
                break;
            }
            case VIDEO_POSITION:
                long _id = mOpenHelper.getWritableDatabase()
                        .insertWithOnConflict(VideoPosition.TABLE_NAME, null, values,
                                SQLiteDatabase.CONFLICT_REPLACE);
                if (_id > 0) {
                    returnUri = VideoPosition.buildVideoPositionUri(_id);
                } else {
                    throw new SQLException("Failed to insert row into " + uri);
                }
                break;
            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }

        mContentResolver.notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        final int rowsDeleted;

        if (selection == null) {
            throw new UnsupportedOperationException("Cannot delete without selection specified.");
        }

        switch (sUriMatcher.match(uri)) {
            case VIDEO: {
                rowsDeleted = mOpenHelper.getWritableDatabase()
                        .delete(VideoEntry.TABLE_NAME, selection, selectionArgs);
                break;
            }
            case VIDEO_POSITION: {
                rowsDeleted = mOpenHelper.getWritableDatabase()
                        .delete(VideoPosition.TABLE_NAME, selection, selectionArgs);
                break;
            }
            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }

        if (rowsDeleted != 0) {
            mContentResolver.notifyChange(uri, null);
        }

        return rowsDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        final int rowsUpdated;

        switch (sUriMatcher.match(uri)) {
            case VIDEO: {
                rowsUpdated = mOpenHelper.getWritableDatabase()
                        .update(VideoEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            }
            case VIDEO_POSITION: {
                rowsUpdated = mOpenHelper.getWritableDatabase()
                        .update(VideoPosition.TABLE_NAME, values, selection, selectionArgs);
                break;
            }
            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }

        if (rowsUpdated != 0) {
            mContentResolver.notifyChange(uri, null);
        }

        return rowsUpdated;
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        switch (sUriMatcher.match(uri)) {
            case VIDEO: {
                final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
                int returnCount = 0;

                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        long _id = db.insertWithOnConflict(VideoEntry.TABLE_NAME, null, value,
                                SQLiteDatabase.CONFLICT_REPLACE);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }

                mContentResolver.notifyChange(uri, null);
                return returnCount;
            }
            default: {
                return super.bulkInsert(uri, values);
            }
        }
    }
}
