/*
 * This file was copied from the Android TV Leanback sample project and modified by Alec Douglas.
 * https://github.com/googlesamples/androidtv-Leanback
 * The original file included the copyright notice below:
 *
 * Copyright (c) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.alecgdouglas.gbenthusiast.data;

import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * VideoContract represents the contract for storing videos in the SQLite database.
 */
public final class VideoContract {

    // The name for the entire content provider.
    public static final String CONTENT_AUTHORITY = "com.alecgdouglas.gbenthusiast";
    // The content paths.
    public static final String PATH_VIDEO = "video";
    public static final String PATH_VIDEO_POSITION = "video_position";
    public final static String PATH_TYPE_NAMES_SORTED_BY_MOST_RECENT =
            "type_names_sorted_by_most_recent";
    // Base of all URIs that will be used to contact the content provider.
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final class VideoPosition implements BaseColumns {
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_VIDEO_POSITION).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "." +
                        PATH_VIDEO_POSITION;

        public static final String TABLE_NAME = "video_position";

        public static final String COLUMN_VIDEO_POSITION_MILLIS = "video_position_millis";

        public static final String COLUMN_VIDEO_ID = "video_id";

        public static final String COLUMN_LAST_UPDATED = "last_updated";

        public static Uri buildVideoPositionUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class VideoEntry implements BaseColumns {
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_VIDEO).build();
        public static final Uri TYPE_NAMES_SORTED_BY_MOST_RECENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_TYPE_NAMES_SORTED_BY_MOST_RECENT)
                        .build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "." + PATH_VIDEO;

        // Name of the video table.
        public static final String TABLE_NAME = "video";

        // The Giant Bomb ID for the video.
        public static final String COLUMN_GIANT_BOMB_ID = "giant_bomb_id";

        // Name of the video.
        public static final String COLUMN_NAME = SearchManager.SUGGEST_COLUMN_TEXT_1;

        // Description of the video.
        public static final String COLUMN_DECK = SearchManager.SUGGEST_COLUMN_TEXT_2;

        // The type name of the video.
        public static final String COLUMN_TYPE_NAME = "type_name";

        // The publish date of the video.
        public static final String COLUMN_PUBLISH_DATE = "publish_date";

        public static final String COLUMN_LENGTH_SECONDS = "length_seconds";

        // The url to the HD quality video content.
        public static final String COLUMN_VIDEO_URL_HD = "video_url_hd";
        // The url to the high quality video content.
        public static final String COLUMN_VIDEO_URL_HIGH = "video_url_high";
        // The url to the low quality video content.
        public static final String COLUMN_VIDEO_URL_LOW = "video_url_low";

        // The url to the tiny image.
        public static final String COLUMN_IMAGE_URL_TINY = "image_url_tiny";
        // The url to the medium image.
        public static final String COLUMN_IMAGE_URL_MEDIUM = "image_url_medium";
        // The url to the super image.
        public static final String COLUMN_IMAGE_URL_SUPER = "image_url_super";
        // The url to the icon image.
        public static final String COLUMN_IMAGE_URL_ICON = "image_url_icon";

        // Returns the Uri referencing a video with the specified id.
        public static Uri buildVideoUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}
