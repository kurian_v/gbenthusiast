package com.alecgdouglas.gbenthusiast.data;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class VideoFetchResultReceiver extends ResultReceiver {
    private final OnFetchCompleteHandler mOnFetchCompleteHandler;

    public VideoFetchResultReceiver(Handler handler,
                                    OnFetchCompleteHandler onFetchCompleteHandler) {
        super(handler);
        mOnFetchCompleteHandler = onFetchCompleteHandler;
    }

    protected void onReceiveResult(int resultCode, Bundle resultData) {
        mOnFetchCompleteHandler.onFetchComplete();
    }

    public interface OnFetchCompleteHandler {
        void onFetchComplete();
    }
}
