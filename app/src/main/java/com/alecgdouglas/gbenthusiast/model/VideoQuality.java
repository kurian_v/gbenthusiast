package com.alecgdouglas.gbenthusiast.model;

public enum VideoQuality {
    HD,
    HIGH,
    LOW;

    public static VideoQuality stepDown(VideoQuality quality) {
        switch (quality) {
            case HD:
                return VideoQuality.HIGH;
            case HIGH:
            default:
                return VideoQuality.LOW;
        }
    }
}
