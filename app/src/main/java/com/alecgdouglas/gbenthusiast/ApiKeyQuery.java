package com.alecgdouglas.gbenthusiast;

import android.content.Context;
import android.util.Log;
import android.util.Pair;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.Normalizer;
import java.util.Map;

public class ApiKeyQuery {
    private static final String TAG = "ApiKeyQuery";

    private static final String GIANT_BOMB_LINK_CODE_URL_STRING =
            "https://www.giantbomb" + "" + ".com/app/gbenthusiast/";
    private static final String VALIDATE_API = "get-result";
    private static final String LINK_CODE_PARAM_NAME = "regCode";
    private static final String API_KEY_NAME = "regToken";

    private ApiGetRequestTask mTask;

    public void run(final Context context, String linkCode, final Callback callback) {
        final String cleanLinkCode = getCleanLinkCode(linkCode);

        final ApiGetRequestParams getApiKeyRequestParams = new ApiGetRequestParams(VALIDATE_API,
                new Pair<>(LINK_CODE_PARAM_NAME, cleanLinkCode));
        mTask = new ApiGetRequestTask() {
            @Override
            protected String getEndpointUrl() {
                return GIANT_BOMB_LINK_CODE_URL_STRING;
            }

            @Override
            protected void onPostExecute(Map<ApiGetRequestParams, JSONObject> responses) {
                final String apiKey =
                        getApiKeyFromJsonResponse(responses.get(getApiKeyRequestParams));
                boolean success = apiKey != null;
                if (!success) {
                    // Wasn't able to get the key at all, report back immediately that we failed.
                    callback.onQueryComplete(false);
                    return;
                }

                // We got the key, now we just need to store it. Do this off the main thread since
                // it involves I/O.
                final ApiKeyManager.OnStoreCompleteCallback onStoreCompleteCallback =
                        new ApiKeyManager.OnStoreCompleteCallback() {
                            @Override
                            public void onStoreComplete(boolean success) {
                                callback.onQueryComplete(success);
                            }
                        };
                ApiKeyManager.storeApiKey(context, apiKey, onStoreCompleteCallback);
            }
        };
        mTask.execute(getApiKeyRequestParams);
    }

    public void cancel() {
        if (mTask != null) {
            mTask.cancel(false);
        }
    }

    public boolean isRunning() {
        return mTask != null && mTask.isRunning();
    }

    private String getCleanLinkCode(String dirtyLinkCode) {
        String normalizedLinkCode = Normalizer.normalize(dirtyLinkCode, Normalizer.Form.NFD);
        String alphaNumLinkCode = normalizedLinkCode.replaceAll("[^A-Za-z0-9]", "");
        return alphaNumLinkCode.toUpperCase();
    }

    /**
     * @return A non-empty string if the key was successfully retrieved from the JSONObject, null
     * otherwise.
     */
    private String getApiKeyFromJsonResponse(JSONObject response) {
        if (response == null) {
            Log.i(TAG, "JSON response to API key request is null");
            return null;
        }

        if (response.isNull(API_KEY_NAME)) {
            Log.i(TAG, "Response did not contain key '" + API_KEY_NAME + "' or the value was null");
            return null;
        }

        String apiKey;
        try {
            apiKey = response.getString(API_KEY_NAME);
        } catch (JSONException e) {
            Log.i(TAG, "Response did not contain key '" + API_KEY_NAME + "'");
            return null;
        }

        if (apiKey.isEmpty()) {
            Log.i(TAG, "Link code provided is invalid, no API key returned");
            return null;
        }

        return apiKey;
    }

    public interface Callback {
        void onQueryComplete(boolean success);
    }
}
