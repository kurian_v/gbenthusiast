package com.alecgdouglas.gbenthusiast;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.alecgdouglas.gbenthusiast.model.VideoQuality;

public class DefaultQualityPrefUtils {
    private static final String TAG = "DefaultQualityPrefUtils";

    private static final String PREF_KEY_DEFAULT_QUALITY = "default_quality";
    private static final VideoQuality INITIAL_DEFAULT_VIDEO_QUALITY = VideoQuality.HD;

    private static VideoQuality sDefaultVideoQuality = null;

    private DefaultQualityPrefUtils() {
    }

    public static void reset(Context context) {
        setDefaultQuality(context, INITIAL_DEFAULT_VIDEO_QUALITY);
    }

    public static VideoQuality getDefaultQuality(Context context) {
        if (sDefaultVideoQuality != null) return sDefaultVideoQuality;

        final SharedPreferences prefs =
                context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        final String qualityString =
                prefs.getString(PREF_KEY_DEFAULT_QUALITY, INITIAL_DEFAULT_VIDEO_QUALITY.toString());
        Log.d(TAG, "Saved quality pref is " + qualityString);

        try {
            return VideoQuality.valueOf(qualityString);
        } catch (IllegalArgumentException | NullPointerException e) {
            Log.w(TAG, "Exception hit while trying to convert saved quality to enum value", e);
            // The saved quality is no good, reset the pref.
            reset(context);
            return INITIAL_DEFAULT_VIDEO_QUALITY;
        }
    }

    public static void setDefaultQuality(Context context, VideoQuality quality) {
        final SharedPreferences prefs =
                context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        prefs.edit().putString(PREF_KEY_DEFAULT_QUALITY, quality.toString()).apply();
        Log.d(TAG, "Default video quality set to " + quality.toString());
        sDefaultVideoQuality = quality;
    }
}
