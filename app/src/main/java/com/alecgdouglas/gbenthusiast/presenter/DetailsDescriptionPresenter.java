/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.alecgdouglas.gbenthusiast.presenter;

import android.support.v17.leanback.widget.AbstractDetailsDescriptionPresenter;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alecgdouglas.gbenthusiast.R;
import com.alecgdouglas.gbenthusiast.model.Video;

public class DetailsDescriptionPresenter extends AbstractDetailsDescriptionPresenter {
    private final String mSubtitleContainerTag = "subtitle_container";
    private final String mPublishDateTextTag = "publish_date_text";
    private final String mDurationTextTag = "duration_text";
    private final String mProgressTextTag = "progress_text";

    @Override
    protected void onBindDescription(ViewHolder viewHolder, Object item) {
        final Video video = (Video) item;

        if (video != null) {
            viewHolder.getTitle().setText(video.getName());

            final LinearLayout textContainer = (LinearLayout) viewHolder.getTitle().getParent();
            LinearLayout subtitleContainer =
                    (LinearLayout) textContainer.findViewWithTag(mSubtitleContainerTag);
            if (subtitleContainer == null) {
                subtitleContainer = new LinearLayout(textContainer.getContext());
                subtitleContainer.setTag(mSubtitleContainerTag);
                subtitleContainer.setOrientation(LinearLayout.HORIZONTAL);
                final LinearLayout.LayoutParams subtitleContainerParams =
                        new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);
                textContainer.addView(subtitleContainer, 1, subtitleContainerParams);
            }

            TextView publishDateText =
                    (TextView) subtitleContainer.findViewWithTag(mPublishDateTextTag);
            if (publishDateText == null) {
                publishDateText = new TextView(textContainer.getContext());
                publishDateText.setTag(mPublishDateTextTag);
                final LinearLayout.LayoutParams publishDateTextParams =
                        new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 3);
                subtitleContainer.addView(publishDateText, publishDateTextParams);
            }
            publishDateText.setText(video.getFormattedPublishDate());

            if (!video.isLive()) {
                TextView durationText =
                        (TextView) subtitleContainer.findViewWithTag(mDurationTextTag);
                if (durationText == null) {
                    durationText = new TextView(textContainer.getContext());
                    durationText.setTag(mDurationTextTag);
                    final LinearLayout.LayoutParams durationTextParams =
                            new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,
                                    1);
                    durationText.setGravity(Gravity.RIGHT);
                    subtitleContainer.addView(durationText, durationTextParams);
                }
                durationText.setText(video.getFormattedDuration());
            }

            TextView progressText = (TextView) subtitleContainer.findViewWithTag(mProgressTextTag);
            if (progressText == null) {
                progressText = new TextView(textContainer.getContext());
                progressText.setTag(mProgressTextTag);
                final LinearLayout.LayoutParams progressTextParams =
                        new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
                progressText.setGravity(Gravity.RIGHT);
                subtitleContainer.addView(progressText, progressTextParams);
            }
            if (video.isLive()) {
                progressText.setText(R.string.live_progress);
            } else {
                progressText.setText(video.getFormattedProgressPercent());
            }

            viewHolder.getBody().setText(video.getDeck());
        }
    }
}
