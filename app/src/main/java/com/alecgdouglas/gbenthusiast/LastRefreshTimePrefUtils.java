package com.alecgdouglas.gbenthusiast;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class LastRefreshTimePrefUtils {
    private static final String TAG = "LastRefreshTimePref";

    private static final String PREF_KEY_LAST_REFRESH_TIME_MS = "last_refresh_time_ms";
    private static final Long SHORT_REFRESH_INTERVAL_MINUTES = 45L;
    private static final Long LONG_REFRESH_INTERVAL_MINUTES = 240L;
    private static final long INVALID_LAST_REFRESH_TIME = -1L;

    // Change the name of this pref to force a full refresh on next refresh after users get the next
    // update.
    private static final String PREF_KEY_LAST_FULL_FETCH_TIME_MS = "last_full_fetch_time_ms_v2";
    private static final long INVALID_FULL_FETCH_TIME = -1L;
    // Data shows that for the past 300 videos, the average number of days between releasing 100
    // videos is 52 days. For the past 200 videos, that average drops to 44 days/100 videos.
    // That's roughly 6 weeks, so we'll be extra cautious and do a full fetch at intervals of 1/3 of
    // that, or 2 weeks.
    private static final long FULL_FETCH_INTERVAL_DAYS = 14L;

    private LastRefreshTimePrefUtils() {
    }

    public static void reset(Context context) {
        update(context, INVALID_LAST_REFRESH_TIME);
    }

    public static void update(Context context) {
        update(context, new Date().getTime());
    }

    private static void update(Context context, long lastRefreshTime) {
        final SharedPreferences prefs =
                context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        prefs.edit().putLong(PREF_KEY_LAST_REFRESH_TIME_MS, lastRefreshTime).apply();
        Log.d(TAG, "Last refresh time pref set to " + lastRefreshTime);
    }

    /**
     * @return True if videos haven't been refreshed in longer than the refresh interval set in
     * [SHORT|LONG]_REFRESH_INTERVAL_MINUTES or have never been refreshed, false otherwise.
     */
    public static boolean shouldRefresh(Context context, boolean useLongInterval) {
        if (!ApiKeyManager.hasApiKey(context)) {
            Log.i(TAG, "No API key stored, should not attempt to refresh");
            return false;
        }
        final SharedPreferences prefs =
                context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        final long lastRefreshTimeMs =
                prefs.getLong(PREF_KEY_LAST_REFRESH_TIME_MS, INVALID_LAST_REFRESH_TIME);
        final Date now = new Date();
        final long minutesSinceLastRefresh =
                TimeUnit.MILLISECONDS.toMinutes(now.getTime() - lastRefreshTimeMs);
        Log.d(TAG, "It's been " + minutesSinceLastRefresh + " minutes since last refresh, " +
                "checking " + "against " + (useLongInterval ? "long" : "short") + " refresh " +
                "interval");
        final long refreshIntervalMinutes =
                useLongInterval ? LONG_REFRESH_INTERVAL_MINUTES : SHORT_REFRESH_INTERVAL_MINUTES;
        if (lastRefreshTimeMs == INVALID_LAST_REFRESH_TIME ||
                minutesSinceLastRefresh >= refreshIntervalMinutes ||
                shouldPerformFullFetch(context)) {
            Log.d(TAG, "Videos should be refreshed");
            return true;
        }
        Log.d(TAG, "Videos should not be refreshed");
        return false;
    }

    /**
     * @return True if videos haven't been refreshed in longer than the refresh interval set in
     * FULL_FETCH_INTERVAL_DAYS or have never been refreshed or the videos database is
     * empty, false otherwise.
     */
    public static boolean shouldPerformFullFetch(Context context) {
        final SharedPreferences prefs =
                context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        final long lastFullFetchTimeMs =
                prefs.getLong(PREF_KEY_LAST_FULL_FETCH_TIME_MS, INVALID_FULL_FETCH_TIME);
        final Date now = new Date();
        final long daysSinceLastFullFetch =
                TimeUnit.MILLISECONDS.toDays(now.getTime() - lastFullFetchTimeMs);
        Log.d(TAG, "It's been " + daysSinceLastFullFetch + " days since last full fetch");
        if (lastFullFetchTimeMs == INVALID_FULL_FETCH_TIME ||
                daysSinceLastFullFetch >= FULL_FETCH_INTERVAL_DAYS) {
            Log.d(TAG, "A full fetch should be performed because it's been greater than " +
                    FULL_FETCH_INTERVAL_DAYS + " days since the last full fetch");
            return true;
        }
        Log.d(TAG, "A full fetch should not be performed");
        return false;
    }

    public static void updateLastFullFetchTime(Context context) {
        final SharedPreferences prefs =
                context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        final long lastFullFetchTime = new Date().getTime();
        prefs.edit().putLong(PREF_KEY_LAST_FULL_FETCH_TIME_MS, lastFullFetchTime).apply();
        Log.d(TAG, "Last full fetch time pref set to " + lastFullFetchTime);
    }
}
